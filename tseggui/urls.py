from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from filebrowser.sites import site
from django.conf.urls.static import static

urlpatterns = [
    # Examples:
    url(r'^$', 'news.views.home', name='home'),
    url(r'^angilal/(?P<pk>\d+)$', 'news.views.angilal_view', name='medee_angilal_view'),
    url(r'^(?P<pk>\d+)/(?P<slug>.{1,300})$', 'news.views.medee_view', name='medee_view'),
    url(r'^tag/(?P<tag>.{1,300})$', 'news.views.medee_by_tag', name='medee_by_tag'),
    url(r'^video_medee$', 'news.views.video_medee', name='video_medee'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tmceeel/', include('tinymce.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
