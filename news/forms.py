# coding:utf-8
import json

from django import forms

import autocomplete_light
from news.models import Medee
from news.models import MedeeComment
from news.utils import get_user_agent
import bleach
import xml.etree.ElementTree as ET

from tinymce.widgets import TinyMCE


class MedeeForm(autocomplete_light.ModelForm):

    class Meta:
        model = Medee
        fields = ("title", "angilal", "zurag", "brief", "content", "is_show", "is_review", "is_ontsloh", "is_nuurend_ontsloh")
        widgets = {
            # 'tags': autocomplete_light.TextWidget('MedeeTagAutocomplete', attrs={"style": "width:300px;"}),
            'content': TinyMCE(attrs={'cols': 80, 'rows': 30}),
        }

    def clean_zurag(self):
        is_ontsloh = self.cleaned_data.get("is_ontsloh")
        is_nuurend_ontsloh = self.cleaned_data.get("is_nuurend_ontsloh")
        zurag = self.cleaned_data.get("zurag", None)
        if is_ontsloh is True and not zurag:
            raise forms.ValidationError(u"Онцлох мэдээ заавал зурагтай байна!!!!!")
        if is_nuurend_ontsloh is True and not zurag:
            raise forms.ValidationError(u"Нүүрэнд онцлох мэдээ заавал зурагтай байна!!!!!")
        return zurag

    def clean_video_code(self):
        video_code = self.cleaned_data.get("video_code")
        if video_code:
            video_code = bleach.clean(
                video_code,
                tags=["iframe"],
                attributes={
                    "iframe": ["src", "frameborder", "allowfullscreen"]}
            )
            tree = ET.fromstring(video_code)
            # root = tree.getroot()
            tree.set("class", "embed-responsive-item")
            tree.text = " "
            video_code = ET.tostring(tree)

        return video_code


class MedeeCommentForm(forms.ModelForm):
    # zurag = CaptchaField()

    class Meta:
        model = MedeeComment
        fields = ("name", "email", "comment")
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Зочин', 'width': '200px', }),
            'comment': forms.Textarea(attrs={"class": "form-control", "rows": 3, 'placeholder': 'Сэтгэгдэл бичих'}),
        }

    def save(self, request, medee):
        comment = super(MedeeCommentForm, self).save(commit=False)
        comment.medee = medee

        if request.user.is_authenticated():
            comment.created_user = request.user
            comment.email = request.user.email

        user_agent = get_user_agent(request)

        comment.remote_ip = request.META.get("REMOTE_ADDR", "??")
        comment.client_browser = str(user_agent)
        comment.client_browser = request.META.get("HTTP_USER_AGENT", "??")
        comment.cookie = json.dumps(request.COOKIES)
        # rotate_token(request)
        # Detect Auto bots
        if user_agent.os.family == "Windows XP" and user_agent.browser.family == "IE" and user_agent.browser.version_string == "6":
            comment.is_show = False
        comment.save()
        return comment
