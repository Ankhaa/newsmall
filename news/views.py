# coding:utf-8
from django.shortcuts import render
import datetime
from django.http import Http404
from django.core.paginator import Paginator
from django.db.models import Count
# from django.http import HttpResponseRedirect
# from django.shortcuts import render
# from django.core.urlresolvers import reverse
from collections import OrderedDict
from news.models import Medee, ShuurhaiMedee, Angilal, MedeeTag

# from django.core.cache import cache
# from django.db.models import Count

# Create your views here.


def home(request):
    sh_medee = ShuurhaiMedee.objects.all().order_by("-created_date")[:4]
    odtoi_medee = Medee.safe_list().select_related("angilal", "created_user").order_by("-post_date").filter(is_odtoi=True)[:4]
    medee_qs = Medee.safe_list().exclude(pk__in=odtoi_medee)
    ontsloh = medee_qs.filter(is_nuurend_ontsloh=True)[:6]
    medee_qs = medee_qs.exclude(pk__in=ontsloh)

    splitter = datetime.datetime.now() - datetime.timedelta(days=100)
    ih_unshsan = Medee.safe_list().filter(post_date__gte=splitter).order_by("-read_count")[:5]
    # cache.set(Medee.CACHE_KEY_MEDEE_TOP_READ, ih_unshsan)

    video_medee = Medee.safe_list().filter(video_code__contains="iframe").order_by("-post_date")[:4]

    # last_medee = Medee.safe_list().order_by("-post_date")[:20]

    new_medee_by_angilal = OrderedDict()

    for a in Angilal.objects.all().annotate(medee_count=Count("medee")).filter(medee_count__gt=0, is_show=True):
        new_medee_by_angilal[a] = medee_qs.filter(angilal=a)[:5]

    medee_qs = medee_qs[:8]

    # new_medee_by_angilal = cache.get(Medee.CACHE_KEY_MEDEE_BY_ANGILAL_FRONT, False)

    # if new_medee_by_angilal is False:
    #     new_medee_by_angilal = OrderedDict()

    #     for a in Angilal.objects.all().annotate(medee_count=Count("medee")).filter(medee_count__gt=0):
    #         new_medee_by_angilal[a] = medee_qs.filter(angilal=a)[:5]
    # cache.set(Medee.CACHE_KEY_MEDEE_BY_ANGILAL_FRONT, new_medee_by_angilal)

    context = {
        # "new_medee_by_angilal": new_medee_by_angilal,
        "ontsloh": ontsloh,
        "sh_medee": sh_medee,
        "odtoi_medee": odtoi_medee,
        "activate_home": True,
        "ih_unshsan": ih_unshsan,
        "video_medee": video_medee,
        "last_medee": medee_qs,
        "new_medee_by_angilal": new_medee_by_angilal,
    }
    return render(request, 'news/index.html', context)


def angilal_view(request, pk, extra_context=None, template=None, number=50):
    try:
        angilal = Angilal.safe_list().get(pk=pk)
    except Angilal.DoesNotExist:
        raise Http404
    qs = Medee.safe_list()

    qs = qs.filter(angilal=angilal)

    ontsloh_2 = qs.filter(is_ontsloh=True)[:3]
    last_medee = Medee.safe_list().order_by("-post_date")[:20]

    qs = qs.exclude(pk__in=ontsloh_2)
    qs = qs.select_related("angilal", "created_user")

    context = {
        "all_medee": qs,
        "last_medee": last_medee,
        "niited_ontsloh": ontsloh_2,
        "view_angilal": angilal,
        # "OG_IMAGE": obj.image,
        "OG_TYPE": "article",
        "TITLE": angilal,
        "angilal": angilal,
        # "META_DESCRIPTION",obj.content
    }
    if extra_context is not None:
        context.update(extra_context)
    return render(request, 'news/angilal_view.html', context)


def medee_view(request, pk, slug, extra_context=None, template=None, number=50):
    try:
        medee = Medee.safe_list().select_related("angilal", "created_user").get(pk=pk, slug=slug)
    except Medee.DoesNotExist:
        raise Http404

    medeenuud = Medee.safe_list()
    medee_unshih = [medee.pk]
    medeenuud = medeenuud.exclude(pk__in=medee_unshih)

    last_medee = Medee.safe_list().order_by("-post_date")[:20]
    # holbootoi_medee = []
    # for rel_medee in medee.tags.similar_objects()[:6]:
    #     holbootoi_medee.append(rel_medee)
    # holbootoi_deer_garsan = [m.pk for m in holbootoi_medee]

    # niited_ontsloh = cache.get(Medee.CACHE_KEY_MEDEE_LAST_NEWS, False)
    # if niited_ontsloh is False:
    #     niited_ontsloh = medeenuud.filter(is_nuurend_ontsloh=True).order_by("-post_date")[:5]
    #     cache.set(Medee.CACHE_KEY_MEDEE_LAST_NEWS, niited_ontsloh, 100)  # 100 sec
    # niited_ontslohd_garsan = [m.pk for m in niited_ontsloh]

    # hasagdah_list = list(chain(holbootoi_deer_garsan, medee_unshih, niited_ontslohd_garsan),)

    # splitter = datetime.datetime.now() - datetime.timedelta(days=7)

    # all_medee = cache.get(Medee.CACHE_KEY_MEDEE_ALL_LIST, False)
    # if all_medee is False:
    #     if splitter > medee.post_date:
    #         all_medee = medeenuud.exclude(pk__in=hasagdah_list).order_by("-post_date")[:200]
    #     else:
    #         all_medee = medeenuud.filter(post_date__lte=medee.post_date).exclude(pk__in=hasagdah_list).order_by('-post_date')[:200]
    #     cache.set(Medee.CACHE_KEY_MEDEE_ALL_LIST, all_medee)

    # if request.method == "POST":
    #     comment_form = MedeeCommentForm(request.POST)
    #     if comment_form.is_valid():
    #         comment_form.save(request, medee=medee)
    #         return HttpResponseRedirect(request.META["PATH_INFO"])
    # else:
    #     comment_form = MedeeCommentForm()

    medee.add_readcount()
    import datetime

    now = datetime.datetime.now()

    context = {
        "medee": medee,
        # "holbootoi_medeenuud": holbootoi_medee,
        # "niited_ontsloh": niited_ontsloh,
        # "comment_form": comment_form,
        # "now": datetime.datetime.now(),
        "OG_IMAGE": medee.zurag,
        "OG_TYPE": "article",
        "TITLE": medee,
        "view_angilal": medee.angilal,
        "META_DESCRIPTION": medee.brief,
        "last_medee": last_medee,
        # "all_medee": all_medee,
        "now": now,
    }
    if extra_context is not None:
        context.update(extra_context)
    return render(request, "news/news_read.html", context)


def medee_by_tag(request, tag, extra_context=None, template=None, number=50):
    tag = MedeeTag.objects.filter(name__iexact=tag).first()
    if tag is None:
        raise Http404
    qs = Medee.safe_list().filter(tags__name=tag.name).order_by('-post_date')

    qs_ontsloh = Medee.safe_list()

    ontsloh_2 = qs_ontsloh.filter(is_ontsloh=True)[:8]

    qs = qs.exclude(pk__in=ontsloh_2)
    qs = qs.select_related("angilal", "created_user")

    context = {
        "all_medee": qs,
        # "OG_IMAGE": obj.image,
        "OG_TYPE": "article",
        "TITLE": tag,
        # "META_DESCRIPTION",obj.content
    }
    if extra_context is not None:
        context.update(extra_context)
    return render(request, "news/angilal_view.html", context)


def video_medee(request, extra_context=None, template=None, number=50):
    video_medee = Medee.safe_list().filter(video_code__contains="iframe").order_by("-post_date")

    last_medee = Medee.safe_list().order_by("-post_date")[:20]

    context = {
        "video_medee": video_medee,
        "last_medee": last_medee,
        # "OG_IMAGE": obj.image,
        "OG_TYPE": "article",
        "TITLE": "Видео Мэдээ",
        # "META_DESCRIPTION",obj.content
    }
    if extra_context is not None:
        context.update(extra_context)
    return render(request, 'news/video_medee.html', context)
