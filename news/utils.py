# coding:utf-8
import random
import datetime
from django.template import Context
from django.template.loader import get_template
from django.db.models import Q, F
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.conf import settings
import pysolr
# from nuudel.models import Zone, Placement, Banner
from django.contrib import admin

from django.utils import six
from django.utils.encoding import force_text
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class BaseInput(object):
    """
    The base input type. Doesn't do much. You want ``Raw`` instead.
    """
    input_type_name = 'base'
    post_process = True

    def __init__(self, query_string, **kwargs):
        self.query_string = query_string
        self.kwargs = kwargs

    def __repr__(self):
        return u"<%s '%s'>" % (self.__class__.__name__, self.__unicode__().encode('utf8'))

    def __str__(self):
        return force_text(self.query_string)

    def prepare(self):
        return self.query_string


class Clean(BaseInput):
    """
    An input type for sanitizing user/untrusted input.
    """
    RESERVED_CHARACTERS = (
        '\\', '+', '-', '&&', '||', '!', '(', ')', '{', '}',
        '[', ']', '^', '"', '~', '*', '?', ':', '/',
    )
    RESERVED_WORDS = (
        'AND',
        'NOT',
        'OR',
        'TO',
    )

    def clean(self, query_fragment):
        """
        Provides a mechanism for sanitizing user input before presenting the
        value to the backend.
        A basic (override-able) implementation is provided.
        """
        if not isinstance(query_fragment, six.string_types):
            return query_fragment

        words = query_fragment.split()
        cleaned_words = []

        for word in words:
            if word in self.RESERVED_WORDS:
                word = word.replace(word, word.lower())

            for char in self.RESERVED_CHARACTERS:
                word = word.replace(char, '\\%s' % char)

            cleaned_words.append(word)

        return ' '.join(cleaned_words)

    def prepare(self):
        query_string = super(Clean, self).prepare()
        return self.clean(query_string)


class ReadOnlyModelAdmin(admin.ModelAdmin):
    """ModelAdmin class that prevents modifications through the admin.

    The changelist and the detail view work, but a 403 is returned
    if one actually tries to edit an object.

    Source: https://gist.github.com/aaugustin/1388243
    """

    actions = None

    def get_readonly_fields(self, request, obj=None):
        return self.fields or [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False

    # Allow viewing objects but not actually changing them
    def has_change_permission(self, request, obj=None):
        if request.method not in ('GET', 'HEAD'):
            return False
        return super(ReadOnlyModelAdmin, self).has_change_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        return False


class MockList(object):
    def __init__(self, total_objs):
        self.total_objs = total_objs

    def __len__(self):
        return self.total_objs - 1

    def count(self):
        return self.total_objs

    def __getitem__(self, index):
        return ""


def get_solr_instance_apps():
    solr = pysolr.Solr(settings.SOLR_FULL_ADDRESS_APPS)
    return solr


def clear_banners_cache():
    from nuudel.models import Banner, Zone

    cache.delete("banners_zones")
    cache.delete("banners_zones_eng")

    for zone in Zone.objects.all():
        cache.delete("banner_placement.%s" % zone.id)

    for banner in Banner.objects.all():
        cache.delete("hbanner.%s" % banner.id)


def w_choice(lst):
    n = random.uniform(0, 1)
    for item, weight, plc in lst:
        if n < weight:
            break
        n = n - weight
    return item, plc


def normalize(lst):
    sum, new_lst = 0., []
    for item, weight, plc in lst:
        sum += weight
    for item, weight, plc in lst:
        new_lst.append([item, weight/sum, plc])
    return new_lst


def gen_banner_code(request, zone_id, site_code):
    zone, hbanner = None, u""

    zones = getattr(request, "banners_zones", None)
    zones_eng = getattr(request, "banners_zones_eng", None)

    if 'banners.clients' not in request.META:
        request.META['banners.clients'] = []

    # Поиск зоны
    zone_id = str(zones_eng[zone_id]) if (zones and zone_id in zones_eng) else str(zone_id)
    try:
        if zone_id.isdigit():
            if zones and int(zone_id) in zones:
                zone = zones[int(zone_id)]
            else:
                zone = Zone.objects.get(id=zone_id).__dict__
        else:
            zone = Zone.objects.get(block_code=zone_id.lower()).__dict__
    except Exception:
        zone = None
        # Создание зоны и сайта если его еще не существует
        if not zone_id.isdigit():
            zone = Zone(
                block_code=zone_id.lower(),
                site=site_code
            )
            zone.save()
            zone = zone.__dict__

    # try:
    #    if zone_id.isdigit(): zone = Zone.objects.get(id=zone_id)
    #    else: zone = Zone.objects.get(english_name=zone_id.lower())
    # except Exception:
    #    if not zone_id.isdigit():
    #        zone = Zone(english_name=zone_id.lower(), name=zone_id.lower())
    #        zone.save()

    if zone:
        placement = cache.get("banner_placement.%s" % zone["id"])
        if not placement:
            dt_now = datetime.datetime.now()
            placement = Placement.objects.filter(
                (Q(begin_date__lte=dt_now) | Q(begin_date__isnull=True)),
                (Q(end_date__gte=dt_now) | Q(end_date__isnull=True)),
                (Q(max_clicks__gt=F('clicks')) | Q(max_clicks=0)),
                (Q(max_shows__gt=F('shows')) | Q(max_shows=0)), zones__id=zone["id"]).select_related().order_by('-id').values()
            cache.set("banner_placement.%s" % zone["id"], list(placement), 60*30)

        count = len(placement)

        banner, plc = False, False
        if count == 1:
            plc = placement[0]
            banner = placement[0]["banner_id"]
        elif count > 1:
            banners = [[t_p["banner_id"], t_p["frequency"], t_p] for t_p in placement]
            banner, plc = w_choice(normalize(banners))

        if banner:
            request.banners_placement_shows.append(plc["id"])

            # Cache баннер
            hbanner = cache.get("hbanner.%s" % banner)
            if not hbanner:
                b = Banner.objects.get(id=banner)
                img_url = b.img_file.url if b.img_file else ""
                swf_url = b.swf_file.url if b.swf_file else ""

                code = u""

                # Flash-баннер
                if b.banner_type == 'f':
                    banner_href = reverse("banners:placement", kwargs={"placement_id": plc["id"]})
                    template = get_template("nbanners/gen_banner_code.html")
                    context = Context({
                        "banner_width": b.width,
                        "banner_height": b.height,
                        "data": u"%s?banner_href=%s" % (swf_url, banner_href),
                        "swf_url": swf_url,
                        "banner_href": banner_href,
                        "img_url": img_url,
                        "foreign_url": b.foreign_url,
                    })
                    code += template.render(context)
                # графические баннеры
                elif b.banner_type == 'g':
                    if b.foreign_url:
                        code += u"<a href=\"%s\" target=\"_blank\" style=\"border-width:0;\">" % reverse("banners:placement", kwargs={"placement_id": plc["id"]})
                    code += u"""<img src="%s" width="%s" height="%s" style="border-width:0;" class="img-responsive" alt="banner"/>""" % (img_url, b.width, b.height)
                    if b.foreign_url:
                        code += u"""</a>"""
                # html баннеры
                elif b.banner_type == 'h':
                    code += b.html_text
                hbanner = code
            else:
                pass

            if hbanner:
                hbanner = u"%s%s%s" % (zone.get("html_pre_banner", ""), hbanner, zone.get("html_after_banner", ""))
            else:
                hbanner = u""
            cache.set("hbanner.%s" % banner, hbanner, 60 * 60 * 2)  # 2 цаг
        return hbanner
    else:
        # Блок олдохгүй бол хоосон
        return u""


from hashlib import md5
from user_agents import parse
import sys
# Small snippet from the `six` library to help with Python 3 compatibility
if sys.version_info[0] == 3:
    text_type = str
else:
    text_type = unicode


def get_cache_key(ua_string):
    # Some user agent strings are longer than 250 characters so we use its MD5
    if isinstance(ua_string, text_type):
        ua_string = ua_string.encode('utf-8')
    return ''.join(['django_user_agents.', md5(ua_string).hexdigest()])


def get_user_agent(request):
    # Tries to get UserAgent objects from cache before constructing a UserAgent
    # from scratch because parsing regexes.yaml/json (ua-parser) is slow
    ua_string = request.META.get('HTTP_USER_AGENT', '')
    key = get_cache_key(ua_string)
    user_agent = cache.get(key)
    if user_agent is None:
        user_agent = parse(ua_string)
        cache.set(key, user_agent)
    return user_agent
