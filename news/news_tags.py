#coding:utf-8
import re
import datetime
import bleach
from django import template

from django.conf import settings
from django.utils.dateformat import format
from django.utils.timezone import now as utc_now
from django.template import defaultfilters
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe


register = template.Library()


def date_filter(value, now=None):
    if isinstance(now, datetime.datetime) is False:
        if settings.USE_TZ is True:
            now = utc_now()
        else:
            now = datetime.datetime.now()

    if isinstance(value, datetime.datetime) is False:
        return ""

    delta = now - value

    if delta.days < 1 and now.day != value.day:
        days_diff = 1
    else:
        days_diff = delta.days

    if delta.days == 0:
        sec = delta.seconds

        if delta.seconds < 60:  # Сек
            datestr = u"Саяхан %d хормын өмнө" % delta.seconds
        elif sec < 60 * 60:  # Мин
            datestr = u"%d минутын өмнө" % int(delta.seconds / 60)
        elif days_diff < 1:
            datestr = format(value, u"Өнөөдөр H цаг i минут")
        else:
            datestr = format(value, u"Өчигдөр H цаг i минут")

    elif days_diff == 1:
        datestr = format(value, u"Өчигдөр H цаг i минут ")
    elif days_diff > 1:
        datestr = format(value, u"Y оны n сарын j")

    return datestr


def date_filter_mini(value, now=None):
    if isinstance(now, datetime.datetime) is False:
        if settings.USE_TZ is True:
            now = utc_now()
        else:
            now = datetime.datetime.now()

    if isinstance(value, datetime.datetime) is False:
        return ""

    delta = now - value

    if delta.days < 1 and now.day != value.day:
        days_diff = 1
    else:
        days_diff = delta.days

    if delta.days == 0:
        sec = delta.seconds

        if delta.seconds < 60:  # Сек
            datestr_mini = u"%d хормын өмнө" % delta.seconds
        elif sec < 60 * 60:  # Мин
            datestr_mini = u"%d минутын өмнө" % int(delta.seconds / 60)
        elif days_diff < 1:
            datestr_mini = format(value, u"Өнөөдөр H:i")
        else:
            datestr_mini = format(value, u"Өчигдөр H:i")

    elif days_diff == 1:
        datestr_mini = format(value, u"Өчигдөр H:i")
    elif days_diff > 1:
        datestr_mini = format(value, u"Y/n/j")

    return datestr_mini


register.filter('date_filter', date_filter)
register.filter('date_filter_mini', date_filter_mini)


def multiple_counter(value, multi):
    count = value * multi
    return count

register.filter('multiple_counter', multiple_counter)


@defaultfilters.stringfilter
def remove_spacify(value, autoescape=None):
    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x
    return re.sub('&'+'nbsp;', ' ', esc(value))
remove_spacify.needs_autoescape = True
register.filter(remove_spacify)


@register.filter
def short_filter(html_text, split_len=40):
    ret_str = remove_spacify(html_text)
    ret_str = defaultfilters.striptags(ret_str)
    ret_str = defaultfilters.truncatewords(ret_str, split_len)

    return ret_str
short_filter.is_safe = True


class BannerNode(template.Node):
    def __init__(self, request, zone_id):
        self.request = template.Variable(request)
        self.zone_id = int(zone_id) if zone_id.isdigit() else zone_id

    def render(self, context):
        return gen_banner_code(self.request.resolve(context), self.zone_id, site_code=settings.NUUDEL_CURRENT_APP)


def do_banner(parser, token):
    try:
        tokens = token.split_contents()
        if tokens.__len__() == 3:
            tag_name, request, zone_id = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError#, "%r tag requires a 2 arguments" % token.contents.split()[0]
    return BannerNode(request, zone_id)


register.tag('banner', do_banner)


# @register.simple_tag
@register.inclusion_tag("common/video_meta.html")
def ndv_media(request):
    VIDEO_DATA_URL = settings.VIDEO_DATA_URL
    VIDEO_STORAGE = settings.VIDEO_STORAGE
    STATIC_URL = settings.STATIC_URL
    VIDEO_ANALYZE_DATA_URL = settings.VIDEO_ANALYZE_DATA_URL
    return {
        "STATIC_URL": STATIC_URL,
        "STORAGE": VIDEO_STORAGE,
        "VIDEO_DATA_URL": VIDEO_DATA_URL,
        "VIDEO_ANALYZE_DATA_URL": VIDEO_ANALYZE_DATA_URL
    }


# @register.simple_tag
@register.inclusion_tag("common/chat_meta.html")
def ndc_media(request):
    STATIC_URL = settings.STATIC_URL
    return {
        "NUUDEL_CHAT_SERVER_ADDRESS": getattr(settings, "CHAT_SERVE_ADDRESS", "/chat"),
        "STATIC_URL": STATIC_URL,
        "request": request,
    }


@register.filter
def strip_ndv(raw_tags):
    # <span class="ndv" ndv="PJM8YXwV" width="860" height="480">Wolves-Fsm9opbeyaM.mp4</span>
    # <iframe width="426" height="240" src="http://localhost/embeded.html/YqMmPLOA" frameborder="0" allowfullscreen></iframe>
    return mark_safe(bleach.clean(
        raw_tags,
        tags=["span", "iframe"],
        attributes={
            "span": ["class", "ndv", "width", "height"],
            "iframe": ["src", "width", "height", "frameborder", "allowfullscreen"]}
    ))
