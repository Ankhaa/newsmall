# coding:utf-8
from django.contrib import admin
from django import template
from filebrowser.settings import ADMIN_THUMBNAIL
from news.models import Medee, Angilal, MedeeTag, MedeeComment, ShuurhaiMedee, Banner
from news.forms import MedeeForm
from news.news_tags import date_filter


class MedeeAdmin(admin.ModelAdmin):
    def image_thumbnail(self, obj):
        try:
            if obj.zurag and obj.zurag.filetype == "Image":
                return '<img src="%s" />' % obj.zurag.version_generate(ADMIN_THUMBNAIL).url
            else:
                return u"(Зураггүй)"
        except:
            return u"Файлын нэр асуудалтай байна."
    image_thumbnail.allow_tags = True
    image_thumbnail.short_description = u"Мэдээний зураг"

    # pre_populated_fields = ()
    list_display = [
        "title",
        "image_thumbnail",
        "angilal",
        "show_tags",
        "read_count",
        "listen_count",
        "ucd_date",
        "brief",
        "post_date",
    ]

    list_filter = [
        "angilal",
        "post_date",
        "is_show",
        "is_ontsloh",
        "is_nuurend_ontsloh",
        # "tags"  # Fix from Taggit
    ]
    # change_list_template = "admin/change_list_filter_sidebar.html"
    # change_list_filter_template = "admin/filter_listing.html"
    date_hierarchy = "created_date"
    form = MedeeForm
    fieldsets = [
        (None, {'fields': ['title', ('angilal', "tags",), "zurag", ]}),
        (u"Хяналт", {'fields': [('post_date', 'is_show', 'is_review'), ("is_ontsloh", "is_nuurend_ontsloh", "is_odtoi"), ("width_content")]}),
        (u"Агуулга", {'fields': ['brief', 'content', 'video_code', ]}),
    ]

    # def has_delete_permission(self, request, obj=None):
    #     # request.user.has_perm
    #     return super(MedeeAdmin, self).has_delete_permission(request, obj=obj)
    def get_actions(self, request):
        if request.user.is_superuser is True:
            return super(MedeeAdmin, self).get_actions(request)

        if request.user.has_perm("medee.limited_own"):
            return None
        return super(MedeeAdmin, self).get_actions(request)

    def get_queryset(self, request):
        queryset = super(MedeeAdmin, self).get_queryset(request)
        if request.user.is_superuser is True:
            return queryset

        if request.user.has_perm("medee.limited_own"):
            queryset = queryset.filter(created_user=request.user)
        return queryset

    def show_tags(self, obj):
        return ", ".join(obj.tags.names())
    show_tags.verbose_name = u"Тагууд"

    def ucd_date(self, obj):
        html = u"""
            %(user)s <br/>
            %(c_date)s <br/>
            %(u_date)s
        """ % {
            "user": unicode(obj.created_user),
            "c_date": date_filter(obj.created_date),
            "u_date": date_filter(obj.updated_date)
        }
        return html
    ucd_date.verbose_name = u"Хяналт"
    ucd_date.allow_tags = True

    def save_model(self, request, obj, form, change):
        if change is False:
            obj.created_user = request.user
        else:
            obj.updated_user = request.user
        obj.save()


class AngilalAdmin(admin.ModelAdmin):
    list_display = ["name", "erembe", "tailbar", "is_show", ]


class ShuurhaiMedeeAdmin(admin.ModelAdmin):
    list_display = ["medee"]


admin.site.register(Medee, MedeeAdmin)
admin.site.register(ShuurhaiMedee, ShuurhaiMedeeAdmin)
admin.site.register(Angilal, AngilalAdmin)


class MedeeTagAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['name', 'description', "is_show", "is_system"]}),
    ]
    search_fields = ['name', "description"]
    list_display = ['name', 'description', "is_show", "is_system"]
    list_filter = ["is_show", "is_system"]
    change_list_template = "admin/change_list_filter_sidebar.html"
    change_list_filter_template = "admin/filter_listing.html"

admin.site.register(MedeeTag, MedeeTagAdmin)


class MedeeCommentAdmin(admin.ModelAdmin):
    fieldsets = [
        (
            u"Үндсэн",
            {
                'fields': ["medee", 'name', 'comment', ]
            }
        ),
        (
            u"Хэрэглэгчийн мэдээлэл",
            {
                'fields': ["email", "user_agent", 'remote_ip', "isp"],
                'classes': ('grp-collapse grp-closed',),
            }
        ),
        (
            u"Хяналт",
            {
                'fields': ['is_show', 'is_reviewed'],
                'classes': "collapsable collapsed",
            }
        ),
        (
            u"Хугацааны мэдээлэл",
            {
                'fields': ['created_date', 'updated_date'],
                'classes': ('grp-collapse grp-closed',),
            }
        ),
    ]
    readonly_fields = [
        "medee",
        'name',
        'comment',
        "user_agent",
        "email",
        "remote_ip",
        "isp",
        "created_date",
        "updated_date",
    ]
    search_fields = ['name', "comment", "remote_ip", "user_agent"]
    date_hierarchy = "created_date"
    list_display = ["medee", 'name', 'comment', "is_show", "is_reviewed", "user_agent", "created_date"]
    list_filter = ["is_show", "is_reviewed"]
    change_list_template = "admin/change_list_filter_sidebar.html"
    change_list_filter_template = "admin/filter_listing.html"

admin.site.register(MedeeComment, MedeeCommentAdmin)
admin.site.register(Banner)
