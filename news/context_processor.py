from news.models import Angilal, Banner, Medee


def medee_angilal(request):
    angilaluud = Angilal.safe_list()[:7]
    angilal_others = Angilal.safe_list()[7:]
    banner_deed = Banner.objects.filter(banner_loc="HEADER_TOP").order_by("-created_date")[:1]
    banner_dundtom = Banner.objects.filter(banner_loc="MIDDLE_LONG").order_by("-created_date")[:1]
    banner_dundjij = Banner.objects.filter(banner_loc="MIDDLE_SMALL").order_by("-created_date")[:1]
    # ih_unshsan = cache.get(Medee.CACHE_KEY_MEDEE_TOP_READ, False)
    # if ih_unshsan is False:
    #     splitter = datetime.datetime.now() - datetime.timedelta(days=3)
    ih_unshsan = Medee.safe_list().order_by("-read_count")[:5]
    #     cache.set(Medee.CACHE_KEY_MEDEE_TOP_READ, ih_unshsan)

    # niited_ontsloh = cache.get(Medee.CACHE_KEY_MEDEE_LAST_NEWS, False)
    # if niited_ontsloh is False:
    #     niited_ontsloh = Medee.safe_list().filter(is_nuurend_ontsloh=True).order_by("-post_date")[:5]
    #     cache.set(Medee.CACHE_KEY_MEDEE_LAST_NEWS, niited_ontsloh, 100)  # 100 sec

    return {
        "angilaluud": angilaluud,
        "angilal_others": angilal_others,
        "banner_deed": banner_deed,
        "banner_dundtom": banner_dundtom,
        "banner_dundjij": banner_dundjij,
        # "niited_ontsloh": niited_ontsloh,
        "ih_unshsan": ih_unshsan,
    }

"""

    rank_video_niitlel_by_author = cache.get(VideoNiitlel.CACHE_KEY_MEDEE_VIDEONIITLEL, False)
    if rank_video_niitlel_by_author is False:
        rank_video_niitlel_by_author = OrderedDict()
        for a in Author.objects.all():
            video_niitleluud = VideoNiitlel.safe_list()
            count = video_niitleluud.filter(author=a).count()
            if count > 0:
                rank_video_niitlel_by_author[a] = video_niitleluud.filter(author=a).order_by("erembe", "-created_date")[:10]
        cache.set(VideoNiitlel.CACHE_KEY_MEDEE_VIDEONIITLEL, rank_video_niitlel_by_author, 60 * 20)
    "top_medeenuud": top_read,
        "latest_news": latest_news,
        "medee_comments": medee_comments,
        "last_niitleluud": last_niitleluud,
        "special_niitleluud": special_niitleluud,
        "read_niitleluud": read_niitleluud,
        "rank_video_niitlel_by_author": rank_video_niitlel_by_author,
"""
