# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import filebrowser.fields
import tinymce.models
from django.conf import settings
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Angilal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=300, verbose_name='\u041d\u044d\u0440')),
                ('erembe', models.PositiveIntegerField(verbose_name='\u042d\u0440\u044d\u043c\u0431\u044d')),
                ('classes', models.CharField(max_length=300, verbose_name='Icon class')),
                ('tailbar', models.TextField(null=True, verbose_name='\u0410\u043d\u0433\u0438\u043b\u0430\u043b\u0434 \u0445\u0430\u043c\u0430\u0430\u0440\u0430\u0445 \u043c\u044d\u0434\u044d\u044d\u043d\u04af\u04af\u0434', blank=True)),
                ('is_show', models.BooleanField(default=True, verbose_name='\u0425\u0430\u0440\u0430\u0433\u0434\u0430\u0445 \u044d\u0441\u044d\u0445')),
            ],
            options={
                'ordering': ['erembe'],
                'verbose_name': '\u041c\u044d\u0434\u044d\u044d\u043d\u0438\u0439 \u0430\u043d\u0433\u0438\u043b\u0430\u043b',
                'verbose_name_plural': '\u041c\u044d\u0434\u044d\u044d\u043d\u0438\u0439 \u0430\u043d\u0433\u0438\u043b\u0430\u043b',
            },
        ),
        migrations.CreateModel(
            name='Medee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300, verbose_name='\u0413\u0430\u0440\u0447\u0438\u0433')),
                ('slug', models.CharField(verbose_name='Slug', max_length=300, editable=False)),
                ('zurag', filebrowser.fields.FileBrowseField(max_length=400, null=True, verbose_name=b'\xd0\x97\xd1\x83\xd1\x80\xd0\xb0\xd0\xb3', blank=True)),
                ('brief', models.TextField(help_text='\u041d\u044d\u0433\u044d\u044d\u0441 2 \u04e9\u0433\u04af\u04af\u043b\u0431\u044d\u0440\u044d\u044d\u0440 \u0438\u043b\u044d\u0440\u0445\u0438\u0439\u043b\u044d\u0445', verbose_name='\u0422\u043e\u0432\u0447 \u0430\u0433\u0443\u0443\u043b\u0433\u0430')),
                ('content', tinymce.models.HTMLField(verbose_name='\u0410\u0433\u0443\u0443\u043b\u0433\u0430')),
                ('video_code', models.CharField(max_length=300, null=True, verbose_name='\u0412\u0438\u0434\u0435\u043e\u043d\u044b \u043a\u043e\u0434', blank=True)),
                ('author', models.CharField(verbose_name='\u0417\u043e\u0445\u0438\u043e\u0433\u0447', max_length=300, editable=False)),
                ('width_content', models.BooleanField(default=False, verbose_name='\u04e8\u0440\u0433\u04e9\u043d \u043c\u044d\u0434\u044d\u044d\u043b\u044d\u043b')),
                ('is_show', models.BooleanField(default=True, verbose_name='\u0425\u0430\u0440\u0430\u0433\u0434\u0430\u0445 \u044d\u0441\u044d\u0445')),
                ('is_ontsloh', models.BooleanField(default=False, help_text='\u0410\u043d\u0433\u0438\u043b\u0430\u043b \u0434\u044d\u044d\u0440 \u043e\u043d\u0446\u043b\u043e\u0445\u043e\u043e\u0440 \u0433\u0430\u0440\u0430\u0445 \u0431\u043e\u043b \u0447\u0435\u043a\u043b\u044d\u043d\u044d', verbose_name='\u0410\u043d\u0433\u0438\u043b\u0430\u043b \u0434\u044d\u044d\u0440 \u043e\u043d\u0446\u043b\u043e\u0445 \u043c\u044d\u0434\u044d\u044d')),
                ('is_nuurend_ontsloh', models.BooleanField(default=False, help_text='\u041d\u04af\u04af\u0440\u044d\u043d\u0434 \u043e\u043d\u0446\u043b\u043e\u0445 \u0434\u044d\u044d\u0440 \u0433\u0430\u0440\u0430\u0445 \u0431\u043e\u043b \u0447\u0435\u043a\u043b\u044d\u043d\u044d', verbose_name='\u041d\u04af\u04af\u0440\u044d\u043d\u0434 \u043e\u043d\u0446\u043b\u043e\u0445 \u043c\u044d\u0434\u044d\u044d')),
                ('is_odtoi', models.BooleanField(default=False, help_text='\u04ae\u043d\u0434\u0441\u044d\u043d \u0436\u0430\u0433\u0441\u0430\u0430\u043b\u0442\u0430\u043d\u0434 \u043e\u043d\u0446\u043b\u043e\u0445', verbose_name='\u041e\u0434\u0442\u043e\u0439 \u043c\u044d\u0434\u044d\u044d')),
                ('mp3_short', filebrowser.fields.FileBrowseField(help_text='\u0424\u0430\u0439\u043b\u044b\u043d \u043d\u044d\u0440\u0438\u0439\u0433 \u041c\u044d\u0434\u044d\u044d\u043d\u0438\u0439 \u0433\u0430\u0440\u0447\u0438\u0433 <\u0413\u0430\u0440\u0447\u0438\u0433>_tovch.mp3 \u0433\u044d\u0441\u044d\u043d \u0434\u04af\u0440\u043c\u044d\u044d\u0440 \u04af\u04af\u0441\u0433\u044d\u0445 \u0445\u044d\u0440\u044d\u0433\u0442\u044d\u0439.', max_length=400, null=True, verbose_name='Mp3 \u0425\u0443\u0440\u0430\u0430\u043d\u0433\u04af\u0439', blank=True)),
                ('is_review', models.BooleanField(default=False, verbose_name='\u0425\u044f\u043d\u0430\u0441\u0430\u043d \u044d\u0441\u044d\u0445')),
                ('post_date', models.DateTimeField(default=datetime.datetime(2016, 1, 31, 8, 27, 11, 430918), null=True, verbose_name='\u041d\u0438\u0439\u0442\u043b\u044d\u0433\u0434\u044d\u0445 \u043e\u0433\u043d\u043e\u043e', blank=True)),
                ('allow_comment', models.BooleanField(default=True, verbose_name='\u0421\u044d\u0442\u0433\u044d\u0433\u0434\u044d\u043b \u0437\u04e9\u0432\u0448\u04e9\u04e9\u0440\u04e9\u0445 \u044d\u0441\u044d\u0445')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0417\u0430\u0441\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e')),
                ('read_count', models.PositiveIntegerField(default=0, verbose_name='\u0423\u043d\u0448\u0438\u0441\u0430\u043d \u0442\u043e\u043e')),
                ('listen_count', models.PositiveIntegerField(default=0, verbose_name='\u0421\u043e\u043d\u0441\u0441\u043e\u043d \u0442\u043e\u043e')),
                ('angilal', models.ForeignKey(verbose_name='\u0410\u043d\u0433\u0438\u043b\u0430\u043b', to='news.Angilal')),
                ('created_user', models.ForeignKey(related_name='oruulsan_medeenuud', editable=False, to=settings.AUTH_USER_MODEL, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u0445\u044d\u0440\u044d\u0433\u043b\u044d\u0433\u0447')),
            ],
            options={
                'ordering': ['-created_date'],
                'verbose_name': '\u041c\u044d\u0434\u044d\u044d',
                'verbose_name_plural': '\u041c\u044d\u0434\u044d\u044d',
                'permissions': (('limited_own', '\u0417\u04e9\u0432\u0445\u04e9\u043d \u04e9\u04e9\u0440\u0438\u0439\u043d \u043c\u044d\u0434\u044d\u044d\u0433 \u0437\u0430\u0441\u0430\u0445 \u044d\u0440\u0445'),),
            },
        ),
        migrations.CreateModel(
            name='Medee2Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content_object', models.ForeignKey(to='news.Medee')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MedeeComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=300, verbose_name='\u041d\u044d\u0440')),
                ('email', models.CharField(max_length=300, null=True, verbose_name='Email', blank=True)),
                ('comment', models.TextField(verbose_name='\u0422\u0430\u0439\u043b\u0431\u0430\u0440')),
                ('is_show', models.BooleanField(default=True, verbose_name='\u0425\u0430\u0440\u0430\u0433\u0434\u0430\u0445 \u044d\u0441\u044d\u0445')),
                ('is_reviewed', models.BooleanField(default=False, verbose_name='\u0425\u044f\u043d\u0430\u0433\u0434\u0441\u0430\u043d \u044d\u0441\u044d\u0445')),
                ('is_report', models.BooleanField(default=False, verbose_name='\u041c\u044d\u0434\u044d\u044d\u043b\u044d\u0433\u0434\u0441\u044d\u043d \u044d\u0441\u044d\u0445')),
                ('report_count', models.PositiveIntegerField(default=0, verbose_name='\u041c\u044d\u0434\u044d\u044d\u043b\u044d\u0433\u0434\u0441\u044d\u043d \u0442\u043e\u043e')),
                ('nemeh_onoo', models.PositiveIntegerField(default=0, verbose_name='\u042d\u0435\u0440\u044d\u0433 \u043e\u043d\u043e\u043e')),
                ('sorog_onoo', models.PositiveIntegerField(default=0, verbose_name='\u0421\u04e9\u0440\u04e9\u0433')),
                ('remote_ip', models.CharField(verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d IP', max_length=40, editable=False)),
                ('user_agent', models.CharField(verbose_name='User Agent', max_length=1000, editable=False)),
                ('isp', models.CharField(verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d IP', max_length=40, null=True, editable=False, blank=True)),
                ('cookie', models.TextField(default=False, verbose_name='Cookie tracking', editable=False)),
                ('client_browser', models.TextField(verbose_name='Browser Info', max_length=1000, editable=False)),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0417\u0430\u0441\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e')),
                ('medee', models.ForeignKey(related_name='setgegdeluud', verbose_name='\u041c\u044d\u0434\u044d\u044d', to='news.Medee')),
            ],
            options={
                'ordering': ['-created_date'],
                'verbose_name': '\u041c\u044d\u0434\u044d\u044d \u0441\u044d\u0442\u0433\u044d\u0433\u0434\u044d\u043b',
                'verbose_name_plural': '\u041c\u044d\u0434\u044d\u044d \u0441\u044d\u0442\u0433\u044d\u0433\u0434\u044d\u043b',
            },
        ),
        migrations.CreateModel(
            name='MedeeTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, verbose_name=b'Name')),
                ('description', models.CharField(max_length=300, null=True, verbose_name='\u0422\u0430\u0439\u043b\u0431\u0430\u0440')),
                ('is_show', models.BooleanField(default=True, verbose_name='\u0425\u0430\u0440\u0430\u0433\u0434\u0430\u0445 \u044d\u0441\u044d\u0445')),
                ('is_system', models.BooleanField(default=False, verbose_name='\u0414\u043e\u0442\u043e\u043e\u0434 \u0442\u0430\u0433')),
            ],
            options={
                'verbose_name': '\u041c\u044d\u0434\u044d\u044d\u043d\u0438\u0439 \u0442\u04af\u043b\u0445\u04af\u04af\u0440 \u04af\u0433\u04af\u04af\u0434',
                'verbose_name_plural': '\u041c\u044d\u0434\u044d\u044d\u043d\u0438\u0439 \u0442\u04af\u043b\u0445\u04af\u04af\u0440 \u04af\u0433\u04af\u04af\u0434',
            },
        ),
        migrations.CreateModel(
            name='ShuurhaiMedee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('medee', models.TextField(help_text='\u041d\u044d\u0433\u044d\u044d\u0441 2 \u04e9\u0433\u04af\u04af\u043b\u0431\u044d\u0440\u044d\u044d\u0440 \u0438\u043b\u044d\u0440\u0445\u0438\u0439\u043b\u044d\u0445', verbose_name='\u041c\u044d\u0434\u044d\u044d\u043d\u0438\u0439 \u0430\u0433\u0443\u0443\u043b\u0433\u0430')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0417\u0430\u0441\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e')),
            ],
            options={
                'ordering': ['-created_date'],
                'verbose_name': '\u0428\u0443\u0443\u0440\u0445\u0430\u0439 \u041c\u044d\u0434\u044d\u044d',
                'verbose_name_plural': '\u0428\u0443\u0443\u0440\u0445\u0430\u0439 \u041c\u044d\u0434\u044d\u044d',
            },
        ),
        migrations.AddField(
            model_name='medee2tag',
            name='tag',
            field=models.ForeignKey(related_name='tagged_items', to='news.MedeeTag'),
        ),
        migrations.AddField(
            model_name='medee',
            name='tags',
            field=taggit.managers.TaggableManager(to='news.MedeeTag', through='news.Medee2Tag', help_text='\u0422\u04af\u043b\u0445\u04af\u04af\u0440 \u04af\u0433\u044d\u044d \u0431\u0438\u0447\u043d\u044d \u04af\u04af.', verbose_name='\u0422\u04af\u043b\u0445\u04af\u04af\u0440 \u04af\u0433'),
        ),
        migrations.AddField(
            model_name='medee',
            name='updated_user',
            field=models.ForeignKey(related_name='zassan_medeenuud', blank=True, editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u0445\u044d\u0440\u044d\u0433\u043b\u044d\u0433\u0447'),
        ),
    ]
