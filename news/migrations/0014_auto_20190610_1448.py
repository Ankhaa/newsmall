# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0013_auto_20190610_1409'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='created_date',
            field=models.DateTimeField(null=True, verbose_name=b'\xd0\x9e\xd1\x80\xd1\x83\xd1\x83\xd0\xbb\xd1\x81\xd0\xb0\xd0\xbd \xd0\xbe\xd0\xb3\xd0\xbd\xd0\xbe\xd0\xbe', blank=True),
        ),
        migrations.AlterField(
            model_name='banner',
            name='height',
            field=models.CharField(default=b'', max_length=100, verbose_name=b'\xd3\xa8\xd0\xbd\xd0\xb4\xd3\xa9\xd1\x80', blank=True),
        ),
        migrations.AlterField(
            model_name='banner',
            name='name',
            field=models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd1\x8d\xd1\x80'),
        ),
        migrations.AlterField(
            model_name='banner',
            name='updated_date',
            field=models.DateTimeField(null=True, verbose_name=b'\xd0\x9e\xd1\x80\xd1\x83\xd1\x83\xd0\xbb\xd1\x81\xd0\xb0\xd0\xbd \xd0\xbe\xd0\xb3\xd0\xbd\xd0\xbe\xd0\xbe', blank=True),
        ),
        migrations.AlterField(
            model_name='banner',
            name='width',
            field=models.CharField(default=b'', max_length=100, verbose_name=b'\xd3\xa8\xd1\x80\xd0\xb3\xd3\xa9\xd0\xbd', blank=True),
        ),
    ]
