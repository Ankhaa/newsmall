# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0016_auto_20200708_1543'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medee',
            name='post_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u041d\u0438\u0439\u0442\u043b\u044d\u0433\u0434\u044d\u0445 \u043e\u0433\u043d\u043e\u043e', null=True),
        ),
    ]
