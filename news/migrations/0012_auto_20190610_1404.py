# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0011_auto_20190506_2125'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e', null=True),
        ),
        migrations.AlterField(
            model_name='banner',
            name='updated_date',
            field=models.DateTimeField(auto_now=True, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e', null=True),
        ),
    ]
