# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0010_auto_20190506_2004'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='banner_loc',
            field=models.CharField(default=b'HEADER_TOP', max_length=20, verbose_name=b'\xd0\x91\xd0\xb0\xd0\xb9\xd1\x80\xd1\x88\xd0\xb8\xd0\xbb', choices=[(b'HEADER_TOP', b'\xd0\x94\xd1\x8d\xd1\x8d\xd0\xb4 \xd1\x82\xd0\xbe\xd0\xbc'), (b'MIDDLE_LONG', b'\xd0\x94\xd1\x83\xd0\xbd\xd0\xb4 \xd1\x82\xd0\xbe\xd0\xbc'), (b'MIDDLE_SMALL', b'\xd0\x94\xd1\x83\xd0\xbd\xd0\xb4 \xd0\xb6\xd0\xb8\xd0\xb6\xd0\xb8\xd0\xb3')]),
        ),
        migrations.AlterField(
            model_name='banner',
            name='banner_type',
            field=models.CharField(default=b'IMAGE', max_length=20, verbose_name=b'\xd0\xa2\xd3\xa9\xd1\x80\xd3\xa9\xd0\xbb', choices=[(b'IMAGE', b'\xd0\x97\xd1\x83\xd1\x80\xd0\xb3\xd0\xb0\xd0\xbd \xd0\xb1\xd0\xb0\xd0\xbd\xd0\xbd\xd0\xb5\xd1\x80'), (b'VIDEO', b'\xd0\x91\xd0\xb8\xd1\x87\xd0\xbb\xd1\x8d\xd0\xb3'), (b'HTML', b'HTML \xd0\xb1\xd0\xb0\xd0\xbd\xd0\xbd\xd0\xb5\xd1\x80')]),
        ),
    ]
