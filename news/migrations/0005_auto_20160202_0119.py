# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0004_auto_20160202_0051'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medee',
            name='post_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 2, 1, 19, 24, 111660), null=True, verbose_name='\u041d\u0438\u0439\u0442\u043b\u044d\u0433\u0434\u044d\u0445 \u043e\u0433\u043d\u043e\u043e', blank=True),
        ),
    ]
