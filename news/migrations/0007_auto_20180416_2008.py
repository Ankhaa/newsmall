# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0006_auto_20180112_1841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medee',
            name='post_date',
            field=models.DateTimeField(default=None, null=True, verbose_name='\u041d\u0438\u0439\u0442\u043b\u044d\u0433\u0434\u044d\u0445 \u043e\u0433\u043d\u043e\u043e', blank=True),
        ),
    ]
