# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0008_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u044d\u0440')),
                ('html', models.TextField(null=True, verbose_name=b'HTML Banner', blank=True)),
                ('zurag', models.ImageField(max_length=400, upload_to=b'uploads/vsurt/%Y/%m/%d', null=True, verbose_name=b'\xd0\x97\xd1\x83\xd1\x80\xd0\xb0\xd0\xb3', blank=True)),
                ('banner_type', models.CharField(default=b'IMAGE', max_length=10, verbose_name=b'\xd0\xa2\xd3\xa9\xd1\x80\xd3\xa9\xd0\xbb', choices=[(b'IMAGE', b'\xd0\x97\xd1\x83\xd1\x80\xd0\xb3\xd0\xb0\xd0\xbd \xd0\xb1\xd0\xb0\xd0\xbd\xd0\xbd\xd0\xb5\xd1\x80'), (b'VIDEO', b'\xd0\x91\xd0\xb8\xd1\x87\xd0\xbb\xd1\x8d\xd0\xb3'), (b'HTML', b'HTML \xd0\xb1\xd0\xb0\xd0\xbd\xd0\xbd\xd0\xb5\xd1\x80')])),
                ('banner_loc', models.CharField(default=b'HEADER_TOP', max_length=10, verbose_name=b'\xd0\x91\xd0\xb0\xd0\xb9\xd1\x80\xd1\x88\xd0\xb8\xd0\xbb', choices=[(b'HEADER_TOP', b'\xd0\x94\xd1\x8d\xd1\x8d\xd0\xb4 \xd1\x82\xd0\xbe\xd0\xbc'), (b'MIDDLE_LONG', b'\xd0\x94\xd1\x83\xd1\x8d\xd0\xb4 \xd1\x82\xd0\xbe\xd0\xbc'), (b'MIDDLE_SMALL', b'\xd0\x94\xd1\x83\xd0\xbd\xd0\xb4 \xd0\xb6\xd0\xb8\xd0\xb6\xd0\xb8\xd0\xb3'), (b'BOTTOM_LONG', b'\xd0\x94\xd0\xbe\xd0\xbe\xd0\xb4 \xd1\x82\xd0\xbe\xd0\xbc'), (b'BOTTOM_SMALL', b'\xd0\x94\xd0\xbe\xd0\xbe\xd0\xb4 \xd0\xb6\xd0\xb8\xd0\xb6\xd0\xb8\xd0\xb3')])),
                ('open_url', models.CharField(default=b'', max_length=200, verbose_name='\u04ae\u0441\u0440\u044d\u0445 \u0445\u0430\u044f\u0433', blank=True)),
                ('width', models.CharField(default=b'', max_length=100, verbose_name='\u04e8\u0440\u0433\u04e9\u043d', blank=True)),
                ('height', models.CharField(default=b'', max_length=100, verbose_name='\u04e8\u043d\u0434\u04e9\u0440', blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u041e\u0440\u0443\u0443\u043b\u0441\u0430\u043d \u043e\u0433\u043d\u043e\u043e')),
            ],
            options={
                'ordering': ['-created_date'],
                'verbose_name': 'Banner',
                'verbose_name_plural': 'Banner',
            },
        ),
        migrations.AlterField(
            model_name='medee',
            name='post_date',
            field=models.DateTimeField(default=None, null=True, verbose_name='\u041d\u0438\u0439\u0442\u043b\u044d\u0433\u0434\u044d\u0445 \u043e\u0433\u043d\u043e\u043e', blank=True),
        ),
    ]
