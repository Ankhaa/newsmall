# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20160131_0918'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='medee',
            name='mp3_short',
        ),
        migrations.AlterField(
            model_name='medee',
            name='post_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 2, 0, 49, 51, 928206), null=True, verbose_name='\u041d\u0438\u0439\u0442\u043b\u044d\u0433\u0434\u044d\u0445 \u043e\u0433\u043d\u043e\u043e', blank=True),
        ),
    ]
