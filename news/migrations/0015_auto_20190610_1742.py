# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0014_auto_20190610_1448'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='height',
            field=models.CharField(default=0, max_length=100, verbose_name=b'\xd3\xa8\xd0\xbd\xd0\xb4\xd3\xa9\xd1\x80', blank=True),
        ),
        migrations.AlterField(
            model_name='banner',
            name='width',
            field=models.CharField(default=0, max_length=100, verbose_name=b'\xd3\xa8\xd1\x80\xd0\xb3\xd3\xa9\xd0\xbd', blank=True),
        ),
    ]
