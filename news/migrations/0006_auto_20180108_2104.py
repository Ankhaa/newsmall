# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0005_auto_20160202_0119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medee',
            name='is_ontsloh',
            field=models.BooleanField(default=False, help_text='\u0410\u043d\u0433\u0438\u043b\u0430\u043b\u0434 \u043e\u043d\u0446\u043b\u043e\u0445\u043e\u043e\u0440 \u0433\u0430\u0440\u0430\u0445 \u0431\u043e\u043b \u0447\u0435\u043a\u043b\u044d\u043d\u044d', verbose_name='\u0410\u043d\u0433\u0438\u043b\u0430\u043b\u0434 \u043e\u043d\u0446\u043b\u043e\u0445 \u043c\u044d\u0434\u044d\u044d'),
        ),
        migrations.AlterField(
            model_name='medee',
            name='post_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 1, 8, 21, 4, 25, 192722), null=True, verbose_name='\u041d\u0438\u0439\u0442\u043b\u044d\u0433\u0434\u044d\u0445 \u043e\u0433\u043d\u043e\u043e', blank=True),
        ),
    ]
