# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0015_auto_20190610_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medee',
            name='post_date',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='\u041d\u0438\u0439\u0442\u043b\u044d\u0433\u0434\u044d\u0445 \u043e\u0433\u043d\u043e\u043e', blank=True),
        ),
    ]
