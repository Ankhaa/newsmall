# coding:utf-8
import hashlib
import datetime
from django.db import models
from django.db.models import F
from django.conf import settings
from django.core.urlresolvers import reverse
from filebrowser.fields import FileBrowseField
from tinymce.models import HTMLField
from taggit.managers import TaggableManager
from taggit.models import ItemBase

from unidecode import unidecode
from django.core.cache import cache
from filebrowser.settings import ADMIN_THUMBNAIL
from django.utils.safestring import mark_safe


# Create your models here.
class Angilal(models.Model):

    name = models.CharField(u"Нэр", max_length=300)
    erembe = models.PositiveIntegerField(u"Эрэмбэ")
    classes = models.CharField(u"Icon class", max_length=300)
    tailbar = models.TextField(u"Ангилалд хамаарах мэдээнүүд", blank=True, null=True)
    is_show = models.BooleanField(u"Харагдах эсэх", default=True)

    class Meta:
        verbose_name = u"Мэдээний ангилал"
        verbose_name_plural = u"Мэдээний ангилал"
        ordering = ["erembe"]

    def __str__(self):
        return self.name

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)

    def get_absolute_url(self):
        return reverse("medee_angilal_view", kwargs={"pk": self.pk})


class MedeeTag(models.Model):
    name = models.CharField(verbose_name='Name', unique=True, max_length=100)
    description  = models.CharField(u"Тайлбар", max_length=300, null=True)
    is_show  = models.BooleanField(u"Харагдах эсэх", default=True)
    is_system  = models.BooleanField(u"Дотоод таг", default=False)

    class Meta:
        verbose_name = u"Мэдээний түлхүүр үгүүд"
        verbose_name_plural = u"Мэдээний түлхүүр үгүүд"

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        tag = super(MedeeTag, self).save(*args, **kwargs)
        return tag

    def get_absolute_url(self):
        return reverse("medee_by_tag", kwargs={"tag": self.name})


class Medee2Tag(ItemBase):
    content_object = models.ForeignKey("Medee")
    tag = models.ForeignKey(MedeeTag, related_name="tagged_items")

    @classmethod
    def tags_for(cls, model, instance=None):
        if instance is not None:
            rel_name = cls.tag_relname()
            return cls.tag_model().objects.filter(**{
                '%s__content_object' % rel_name: instance
            })
        return cls.tag_model().objects.filter(**{
            '%s__content_object__isnull' % cls.tag_relname(): False
        }).distinct()

    def save(self, *args, **kwargs):
        medee2tag = super(Medee2Tag, self).save(*args, **kwargs)
        return medee2tag

from django.utils.timezone import now
class Medee(models.Model):
    # To nuudel core
    data_type = "medee"
    source_app = "medee"

    CACHE_KEY_MEDEE_TOP_READ = "MEDEE_TOP_READ"
    CACHE_KEY_MEDEE_TOP_COMMENTS = "MEDEE_TOP_COMMENTS"
    CACHE_KEY_MEDEE_LAST_NEWS = "MEDEE_LAST_NEWS"
    CACHE_KEY_MEDEE_ALL_LIST = "CACHE_KEY_MEDEE_ALL_LIST"
    CACHE_KEY_MEDEE_BY_RELATED_LIST = "MEDEE_BY_RELATED_LIST"
    CACHE_KEY_ONTSLOH_LIST = "CACHE_KEY_ONTSLOH_LIST"

    title = models.CharField(u"Гарчиг", max_length=300)
    slug = models.CharField(u"Slug", max_length=300, editable=False)
    angilal = models.ForeignKey(Angilal, verbose_name=u"Ангилал", )
    zurag = FileBrowseField("Зураг", max_length=400, directory="medee/", format="image", blank=True, null=True)
    brief = models.TextField(u"Товч агуулга", help_text=u"Нэгээс 2 өгүүлбэрээр илэрхийлэх")
    content = HTMLField(u"Агуулга", )
    video_code = models.TextField(u"Видеоны код", blank=True, null=True)

    author = models.CharField(u"Зохиогч", max_length=300, editable=False)
    width_content = models.BooleanField(u"Өргөн мэдээлэл", default=False)
    is_show = models.BooleanField(u"Харагдах эсэх", default=True)
    is_ontsloh = models.BooleanField(u"Ангилалд онцлох мэдээ", help_text=u"Ангилалд онцлохоор гарах бол чеклэнэ", default=False)
    is_nuurend_ontsloh = models.BooleanField(u"Нүүрэнд онцлох мэдээ", help_text=u"Нүүрэнд онцлох дээр гарах бол чеклэнэ", default=False)
    is_odtoi = models.BooleanField(u"Одтой мэдээ", help_text=u"Үндсэн жагсаалтанд онцлох", default=False)

    is_review = models.BooleanField(u"Хянасан эсэх", default=False)
    post_date = models.DateTimeField(u"Нийтлэгдэх огноо", null=True, blank=True, default=now,)

    allow_comment = models.BooleanField(u"Сэтгэгдэл зөвшөөрөх эсэх", default=True)

    created_date = models.DateTimeField(u"Оруулсан огноо", auto_now_add=True)
    created_user = models.ForeignKey(settings.MEDEE_AUTH_USER_MODEL, verbose_name=u"Оруулсан хэрэглэгч", editable=False, related_name="oruulsan_medeenuud")
    updated_date = models.DateTimeField(u"Зассан огноо", auto_now=True)
    updated_user = models.ForeignKey(settings.MEDEE_AUTH_USER_MODEL, verbose_name=u"Оруулсан хэрэглэгч", related_name="zassan_medeenuud", editable=False, blank=True, null=True)

    read_count   = models.PositiveIntegerField(u"Уншисан тоо", default=0)
    listen_count   = models.PositiveIntegerField(u"Сонссон тоо", default=0)
    tags         = TaggableManager(u"Түлхүүр үг", through=Medee2Tag, help_text=u"Түлхүүр үгээ бичнэ үү.")

    # def add_readcount(self):
    #     return self.read_count + 1

    def add_readcount(self):

        rcount = self.read_count
        self.read_count = F("read_count") + 1

        self.save(update_fields=["read_count", "updated_date"])
        self.read_count = rcount

    @property
    def is_video(self):
        if self.video_code:
            return True
        else:
            return False

    def get_video_code(self):
        if self.video_code:
            # """<iframe frameborder="0" src="https://video.nuudel.mn/embeded.html/6GZz9ZAK"></iframe>"""
            return mark_safe(self.video_code)

        return ""

    def add_listencount(self):
        lcount = self.listen_count
        self.listen_count = F("listen_count") + 1
        self.save(read_count_deerees=True, update_fields=["listen_count", "updated_date"])
        self.listen_count = lcount

    def angilal_data(self):
        return {
            "category_id": self.angilal_id,
            "name": self.angilal.__unicode__(),
        }

    def comment_count(self):
        return self.setgegdeluud.all().count()

    def safe_comment_list(self):
        return self.setgegdeluud.all().filter(is_show=True)

    @classmethod
    def safe_list(cls):
        dt_now = datetime.datetime.now()
        return cls.objects.filter(is_show=True, post_date__lt=dt_now)

    @classmethod
    def safe_public_list(cls):
        """Олон нийтэд гарна"""
        return cls.safe_list()

    def medee_zurag(self):
        if self.zurag:
            return self.zurag.url

    def save(self, *args, **kwargs):
        read_count_deerees = kwargs.pop("read_count_deerees", False)
        if not self.slug:
            h = hashlib.new('ripemd160')

            h.update(unidecode(self.title))
            self.slug = h.hexdigest()[:10]
        if read_count_deerees is True:
            cache.delete(self.CACHE_KEY_MEDEE_TOP_READ)
        # else:
        #     cache.delete(self.CACHE_KEY_MEDEE_LAST_NEWS)
        #     cache.delete(self.CACHE_KEY_MEDEE_ALL_LIST)
        #     cache.delete(self.CACHE_KEY_MEDEE_TOP_COMMENTS)

        return super(Medee, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u"Мэдээ"
        verbose_name_plural = u"Мэдээ"
        ordering = ["-created_date"]
        permissions = (
            ("limited_own", u"Зөвхөн өөрийн мэдээг засах эрх"),
        )

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "%s" % reverse("medee_view", kwargs={"slug": self.slug, "pk": self.pk})

    def image_thumbnail(self):
        if self.zurag:
            img_url = self.zurag.version_generate(ADMIN_THUMBNAIL).url
            return img_url
        else:
            return False


class MedeeComment(models.Model):
    # Basic
    medee = models.ForeignKey(Medee, verbose_name=u"Мэдээ", related_name="setgegdeluud")
    name = models.CharField(u"Нэр", max_length=300)
    email = models.CharField(u"Email", max_length=300, null=True, blank=True)
    comment = models.TextField(u"Тайлбар",)

    # Administration
    is_show = models.BooleanField(u"Харагдах эсэх", default=True)
    is_reviewed = models.BooleanField(u"Хянагдсан эсэх", default=False)
    is_report = models.BooleanField(u"Мэдээлэгдсэн эсэх", default=False)
    report_count = models.PositiveIntegerField(u"Мэдээлэгдсэн тоо", default=0)

    # Rating
    nemeh_onoo = models.PositiveIntegerField(u"Эерэг оноо", default=0)
    sorog_onoo = models.PositiveIntegerField(u"Сөрөг", default=0)

    # Security Tracking
    remote_ip = models.CharField(u"Оруулсан IP", max_length=40, editable=False)
    user_agent = models.CharField(u"User Agent", max_length=1000, editable=False)
    isp = models.CharField(u"Оруулсан IP", max_length=40, null=True, blank=True, editable=False)
    cookie = models.TextField(u"Cookie tracking", default=False, editable=False)
    client_browser = models.TextField(u"Browser Info", max_length=1000, editable=False)

    # Change log
    created_date = models.DateTimeField(u"Оруулсан огноо", auto_now_add=True)
    updated_date = models.DateTimeField(u"Зассан огноо", auto_now=True)

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)

    class Meta:
        verbose_name = u"Мэдээ сэтгэгдэл"
        verbose_name_plural = u"Мэдээ сэтгэгдэл"
        ordering = ["-created_date"]


class ShuurhaiMedee(models.Model):
    # Basic
    medee = models.TextField(u"Мэдээний агуулга", help_text=u"Нэгээс 2 өгүүлбэрээр илэрхийлэх")
    created_date = models.DateTimeField(u"Оруулсан огноо", auto_now_add=True)
    updated_date = models.DateTimeField(u"Зассан огноо", auto_now=True)

    @classmethod
    def safe_list(cls):
        return cls.objects.all()

    class Meta:
        verbose_name = u"Шуурхай Мэдээ"
        verbose_name_plural = u"Шуурхай Мэдээ"
        ordering = ["-created_date"]


class Banner(models.Model):
    BANNER_IMAGE = "IMAGE"
    BANNER_VIDEO = "VIDEO"
    BANNER_HTML = "HTML"
    BANNER_TYPES = (
        (BANNER_IMAGE, "Зурган баннер"),
        (BANNER_VIDEO, "Бичлэг"),
        (BANNER_HTML, "HTML баннер"),
    )
    LOC_HTOP = "HEADER_TOP"
    LOC_MLONG = "MIDDLE_LONG"
    LOC_MSMALL = "MIDDLE_SMALL"
    BANNER_LOCATION = (
        (LOC_HTOP, "Дээд том"),
        (LOC_MLONG, "Дунд том"),
        (LOC_MSMALL, "Дунд жижиг"),
    )
    name = models.CharField("Нэр", max_length=255, null=False, blank=False)
    html = models.TextField(verbose_name="HTML Banner", null=True, blank=True)
    zurag = models.ImageField("Зураг", max_length=400, blank=True, null=True, upload_to="uploads/vsurt/%Y/%m/%d")
    banner_type = models.CharField("Төрөл", choices=BANNER_TYPES, default=BANNER_IMAGE, max_length=20)
    banner_loc = models.CharField("Байршил", choices=BANNER_LOCATION, default=LOC_HTOP, max_length=20)

    # IMAGE BANNER
    open_url = models.CharField(max_length=200, blank=True, verbose_name=u"Үсрэх хаяг", default="")
    width = models.CharField("Өргөн", blank=True, default=0, null=False, max_length=100)
    height = models.CharField("Өндөр", blank=True, null=False, default=0, max_length=100)

    # Change log
    created_date = models.DateTimeField("Оруулсан огноо", null=True, blank=True)
    updated_date = models.DateTimeField("Оруулсан огноо", null=True, blank=True)

    class Meta:
        verbose_name = "Banner"
        verbose_name_plural = "Banner"
        ordering = ["-created_date"]

    def __unicode__(self):
        return "%s [%s] " % (self.name, self.get_banner_type_display())
